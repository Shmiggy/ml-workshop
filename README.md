# ML Workshop

## The workflow that was followed
0. Lets setup the project.
    - `npm init`
    - Let's make sure we synchronize our functions. (async main)
1. What is the data.
    - Show the text document.
    - What is this data supposed to mean?
2. What does it mean to clean the data.
    - The time axis is split into month and year, can we merge it? (mention this)
3. What is a CSV file.
    - Let's move the data to a CSV file format.
4. How do we load this "csv" data.
    - How do we ingest this data.
    - Why did we think CSV is a good idea?
    - `npm i -s fs` (file stream - loading data from disk)
    - `npm i -s csv-parser` (file format information so we can stop caring about that)
5. How can we visualize this data now that we loaded it.
    - Can this data be plotted as a 2D graph?
    - Let's merge the month and year?
    - Have you heard of plot.ly? We're going to use their API to plot our data.
    - `npm i -s plotly` (plot.ly cloud API)
6. Have you heard of TensorFlow?
    - Use documentation link.
	- `npm i @tensorflow/tfjs-node` (node TensorFlow modules - CPU)
7. What is a tensor?
    - They are a generalization of vectors and matrices to potentially higher dimensions.
8. Can we optimize the data further?
    - Yes, we should normalize the values!
9. What is a normalize function?
    - Normalize: Re-map the tensor into the `[0, 1]` interval.
    - Display mapping: Re-map the tensor to be easier to display.
10. How do we use pseudo-random?
    - Keep the seed consistent.
11. What was the line equation again? (predict by example)
    - What is a `variable` in TensorFlow?
    - How do we do this using tensors? (a * x + b = 0)
12. How do we know how well we're doing?
    - Calculate the loss (mean square root)
13. How do we train?
    - What is an optimizer?
    - How do we use it? (minimize the error by predicting values and computing the MSE iteratively)
    - All TensorFlow `variables` get updated during the minimization process.
14. Let's draw the line, from where to where?
    - We can use 2 points to define a line.
    - We can use an interval to make it easier to see intermediate values.
15. What does this achieve?
    - Is global warming real, or is it a hoax?
    - Are we right though?

## Links
0. Link to MNIST handwritten digit recognition tutorial:
	- https://codelabs.developers.google.com/codelabs/tfjs-training-classfication/index.html?index=..%2F..io2019#0
1. TensorFlow API documentation:
	- https://js.tensorflow.org/api/1.1.2/
2. Plot.ly API:
    - https://plot.ly/nodejs/axes/
    - https://plot.ly/nodejs/legend/